#!/usr/bin/python

# Defnieren jeder bisher bekannten variablen
i = 12
string = "Hallo Welt!"
liste = [1,2,3,4,5]
tumple = (6,7,8,9,10)
gleit = 12.4

print "Simple ausgabe"
print "Mit einem Integer wert " + str(i) + " den wir Definiert haben, kann man viel machen"
print "Wir koennen z.b. rechnen: i + 12 = "
print i+12

print "----------------------------------"

print "auch mit unserem string \"" + string + "\" koennen wir viel anstellen!"
string += " Schlechte Welt!"
print "Wir koennen den String erweitern: " + string
string = "Oder ueberschreiben"
print string
print string.upper()
print string.title()
findR = string.find("r")
print findR
print string
string = "Od-der--ue-ber-sc-hrei-ben"
neueListe = string.split("-")
print neueListe
print string.partition("-")

print "----------------------------------"

print liste
print liste[1]
for i in liste:
	print i
liste.reverse()
for i in liste:
	print i
liste.append("huhu")
print liste
liste.remove(2)
print liste
liste.pop(2)
print liste
liste.remove("huhu")
liste.append(3)
liste.sort()
print liste

