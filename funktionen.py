#!/usr/bin/python
# -*- coding: utf-8 -*-

# Funktionen möglichkeiten

def trenner():
	print("################################### TRENNER ###################################")

def summeEasy(a, b, c):
	print(summeEasy.__name__)
	return a + b +c

def summeOptional(a, b, c=0, d=0):
	print(summeOptional.__name__)
	return a + b + c + d

def summeKomplex(a, b, *weitere):
	print(summeKomplex.__name__)
	a = a + b
	print(type(weitere))
	for i in weitere:
		a += i
	return a

def summeMoreKomplex(a, b, **weitere):
	print(summeMoreKomplex.__name__)
	a += b
	print(type(weitere))
	for i in weitere:
		a += weitere[i]
	return a

def summeWhyKeyWords(a=1, b=2, c=3):
	print(summeWhyKeyWords.__name__)
	return a + b + c

x = summeEasy(1, 2, 3)
print(x)
trenner()

x = summeOptional(1, 2)
print(x)
trenner()

x = summeOptional(1, 2, 3)
print(x)
trenner()

x = summeOptional(1, 2, d=3, c=4)
print(x)
trenner()

x = summeKomplex(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
print(x)
trenner()

x = summeMoreKomplex(1, 2, hallo=3, weiter=4)
print(x)
trenner()

x = summeWhyKeyWords(b=10)
print(x)
trenner()

x = summeWhyKeyWords(c=2, b=4)
print(x)
trenner()

x = summeWhyKeyWords(1,2)
print(x)
trenner()
