#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function

class TicTacToeOO():

	def __init__(self, spieler1="x", spieler2="o"):
		self.spieler1 = spieler1
		self.spieler2 = spieler2	
		felder = ("ol", "om", "or", "ml", "mm", "mr", "ul", "um", "ur")
		self.feld = {}
		for i in range(0,9):
			self.feld[felder[i]] = " "		
		self.runden = 0

	def spielerForm(self, spieler=1, form="x"):
		if spieler == 1:
			self.spieler1 = form
		else:
			self.spieler2 = form

	def zug(self, spieler=1, koordinate=""):
		if koordinate != "":
			if spieler == 1:
				self.feld[koordinate] = self.spieler1
			else:
				self.feld[koordinate] = self.spieler2
		else:
			spieler = input("Welchen Spieler? [1,2]: ")
			self.moeglichkeiten()
			koordinate = raw_input("Wohin setzen: ")
		
			if spieler == 1:
				self.feld[koordinate] = self.spieler1
			else:
				self.feld[koordinate] = self.spieler2

	def zeigeFeld(self):
		print(self.feld["ol"], end="")
		print("'", end="")
		print(self.feld["om"], end="")
		print("'", end="")
		print(self.feld["or"], end="\n")
		for i in range(0, 6):
			print("-", end="")
		print("\n", end="")
		print(self.feld["ml"], end="")
		print("'", end="")
		print(self.feld["mm"], end="")
		print("'", end="")
		print(self.feld["mr"], end="\n")
		for i in range(0, 6):
			print("-", end="")
		print("\n", end="")
		print(self.feld["ul"], end="")
		print("'", end="")
		print(self.feld["um"], end="")
		print("'", end="")
		print(self.feld["ur"], end="\n")

	def moeglichkeiten(self):
		print("Möglichkeiten: ", end="")
		for i in self.feld.items():
			if i[1] == " ":
				print(i[0] + ", ", end="")
		print("\n", end="")
						
	def pruefeGewinner(self):
		# 8 Möglichkeiten prüfen
		# Horizontal: obere reihe, mittlere, untere
		# Vertikal: linke riehe, mittlere, rechte
		# Diagonal: Von oben links nach unten rechts, von oben rechts, nach unten Links

		# Prüfe ob "x" gewonnen hat
		master = (self.spieler1, self.spieler2)
		value = True
		for i in range(0,2):
			if self.feld["ol"] == master[i] and self.feld["om"] == master[i] and self.feld["or"] == master[i]:
				value = False
				break
			elif self.feld["ml"] == master[i] and self.feld["mm"] == master[i] and self.feld["mr"] == master[i]:
				value = False
				break
			elif self.feld["ul"] == master[i] and self.feld["um"] == master[i] and self.feld["ur"] == master[i]:
				value = False
				break
			elif self.feld["ol"] == master[i] and self.feld["mm"] == master[i] and self.feld["ur"] == master[i]:
				value = False
				break
			elif self.feld["or"] == master[i] and self.feld["mm"] == master[i] and self.feld["ul"] == master[i]:
				value = False
				break
			else:
				value = True
				if self.runden >= 9:
					value = False
	
		self.runden += 1

		return value

