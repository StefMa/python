#!/usr/bin/python

# Definieren einer Methode
def trenner():
	print("##########")
	print("Trenner")
	print("##########")	
	return

# Spiel
ergebnis = 0
erraten = 1337
while ergebnis != erraten:
	ergebnis = int(input("Eingabe: "))
	if ergebnis == 0:
		print("Abgebrochen.. Schade eigentlich :D")
		break
else:
	print("Super, alles richtig gemacht :D")

trenner()

# Eine for-schleife.
# Geht von 0 bis 10, wobei +2 gezaehlt wird
for i in range(0, 10, 2):
	print(i)

trenner()

# i bekommt den wert der Liste nacheinander
for i in [1,2,3,4]:
	print(i)

trenner()

# Gibt die groesste Zahl aus
print max([1,2,3,4,9])
	
