#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function

# Funktion: Spielfeld ausgeben
def printField(field):
	print(field["ol"], end="")
	print("'", end="")
	print(field["om"], end="")
	print("'", end="")
	print(field["or"], end="\n")
	for i in range(0, 6):
		print("-", end="")
	print("\n", end="")
	print(field["ml"], end="")
	print("'", end="")
	print(field["mm"], end="")
	print("'", end="")
	print(field["mr"], end="\n")
	for i in range(0, 6):
		print("-", end="")
	print("\n", end="")
	print(field["ul"], end="")
	print("'", end="")
	print(field["um"], end="")
	print("'", end="")
	print(field["ur"], end="\n")

	print("Möglichkeiten: ", end="")
	for i in field.items():
		if i[1] == " ":
			print(i[0] + ", ", end="")
	print("\n", end="")

def pruefeGewinner(field):
	# 8 Möglichkeiten prüfen
	# Horizontal: obere reihe, mittlere, untere
	# Vertikal: linke riehe, mittlere, rechte
	# Diagonal: Von oben links nach unten rechts, von oben rechts, nach unten Links

	# Prüfe ob "x" gewonnen hat
	master = ("x", "o")
	value = True
	for i in range(0,2):
		if field["ol"] == master[i] and field["om"] == master[i] and field["or"] == master[i]:
			value = False
			break
		elif field["ml"] == master[i] and field["mm"] == master[i] and field["mr"] == master[i]:
			value = False
			break
		elif field["ul"] == master[i] and field["um"] == master[i] and field["ur"] == master[i]:
			value = False
			break
		elif field["ol"] == master[i] and field["mm"] == master[i] and field["ur"] == master[i]:
			value = False
			break
		elif field["or"] == master[i] and field["mm"] == master[i] and field["ul"] == master[i]:
			value = False
			break
		else:
			value = True
	return value

spieler = "x"
gewonnen = True
zuege = 0

# Create field
felder = ("ol", "om", "or", "ml", "mm", "mr", "ul", "um", "ur")
field = {}
for i in range(0,9):
	field[felder[i]] = " "

printField(field)

# Eingaben
while gewonnen:
	if zuege != 9:
		eingabe = raw_input("Wohin \"" + spieler + "\" setzten: ")
		if eingabe in felder:
			field[eingabe] = spieler
			if spieler == "x":
				spieler = "o"
			else:
				spieler = "x"
		printField(field)
		zuege += 1
	else:
		print("Spiel beendet! - Kein gewinner :(")
		break;
	gewonnen = pruefeGewinner(field)
else:
	print("Glückwunsch! :D")
