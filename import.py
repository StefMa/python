#!/usr/bin/python
# -*- coding: utf-8 -*-

# Import varianten
# Einfacher import
import math
# Einfacher import mit Namensgebung
import sys as more
# Nur bestimmte Module aus einem Import
from random import randint
# Eigenes Modul einbinden
import myawesomemodul

print(math.pi)
print(more.__name__)
print(randint(1,10))
print("This cames from my awesome-modul: " + myawesomemodul.awesomefunction())
