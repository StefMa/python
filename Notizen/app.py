#!/usr/bin/python
# -*- coding: utf-8 -*-

from Basic import Basics
 
main = Basics()
 
note = 1
while note != "0":
	note = raw_input("Notiz:\n")
	main.saveNote(note)
else:
  	save = raw_input("Moechten Sie nun die Notizen speichern? [j][J][n][N]?\n")
	if save == "j" or save == "J":
    	# Save 
		main.saveOnHD()
    	print("Saved")
  
main.printAll()
 
main.readFromHD()
