#!/usr/bin/python
# -*- coding: utf-8 -*-

class Color():
	def create(self, textColor = "", backgroundColor = ""):
		txColor = 30
		bgColor = 30

		textColor = textColor.lower()
		backgroundColor = backgroundColor.lower()

		if textColor == "black":
			txColor = 30
		elif textColor == "red":
			txColor = 31
		elif textColor == "green":
			txColor = 32
		elif textColor == "yellow":
			txColor = 33
		elif textColor == "blue":
			txColor = 34
		elif textColor == "purple":
			txColor = 35
		elif textColor == "cyan":
			txColor = 36
		elif textColor == "white":
			txColor = 37

		if backgroundColor == "black":
			bgColor = 40
		elif backgroundColor == "red":
			bgColor = 41
		elif backgroundColor == "green":
			bgColor = 42
		elif backgroundColor == "yellow":
			bgColor = 43
		elif backgroundColor == "blue":
			bgColor = 44
		elif backgroundColor == "purple":
			bgColor = 45
		elif backgroundColor == "cyan":
			bgColor = 46
		elif backgroundColor == "white":
			bgColor = 47

		return "\033["+str(bgColor)+";"+str(txColor)+"m"

	END = '\033[0m'

