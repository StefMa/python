#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
from Color import Color

class Player():
	def __init__(self):
		self.C = Color()

	def createPlayer(self):
		i = True
		print "Erstelle neuen Spieler"
		race = input("Was für eine Rasse möchtest du spielen?\n[0]Ork\n[1]Mensch\n[2]Magier\n[3]Bogenschütze\n")
		self.setRace(race)

		if i:
			self.level = 1
			self.special = True
			self.magierValue = [0, 0]
			self.firstStrike = False
			self.setName()
			self.setAttack(random.randint(5, 10))
			self.setDefensive(random.randint(10, 15))

			print self.C.create("green", "red") + "Hallo " + self.getName() + " willkommen bei Magic"
			print "Du spielst nun einen " + str(self.getRace()) + " auf Level " + str(self.getLevel())
			print "Dein Angriff ist " + str(self.getAttack()) + " und deine Verteidigung " + str(self.getDefensive()) + self.C.END

	def upgradePlayer(self):
		which = input("Was möchtest du Upgrade?\n[0]Angriff\n[1]Verteidigung\n")
		if which == 0:
			self.setAttack(self.getAttack()+random.randint(2,4))
			return self.getAttack()
		elif which == 1:
			self.setDefensive(self.getDefensive()+random.randint(3,5))
			return self.getDefensive()
		else:
			print "Falsche auswahl..Abbruch"

	def specialAttack(self):
		if self.getSpecialAttack():
			race = self.getRace()
			if race == "Ork":
				print "Spezial-Attacke ist die Horde"
				print "Du bekommst je nach Leven +5 Angriff"
			elif race == "Mensch":
				print "Spezial-Attacke ist Schwert"
				print "Du bekommst Erstschlag und +10 Angriff (Erstschlag)"
			elif race == "Magier":
				print "Spezial-Attacke ist Zaubern"
				print "Du kannst dir Angriff und Verteidigung mit einer Zufälligenzahl erhöhen"
			elif race == "Bogenschütze":
				print "Spezial-Attacke ist Verstecken"
				print "Du bekommst je nach Level +5 Verteidigung"

			activate = input("[0]Aktivieren?\n[1]Jetzt nicht\n")
			if activate == 0:
				return True
			else:
				return False
		else:
			print "Du hast deine Spezial-Attacke schon aktiviert..."

	def getFirstStrike(self):
		return self.firstStrike

	def setFirstStrike(self, value):
		self.firstStrike = value

	def getSpecialAttack(self):
		if self.special:
			return True
		else:
			return False

	def setSpecialAttack(self):
		race = self.getRace()
		if race == "Ork":
			self.setAttack(self.getAttack() + 5*self.getLevel())
			self.setDefensive(self.getDefensive())
			self.setFirstStrike(False)
		elif race == "Mensch":
			self.setAttack(self.getAttack() + 10*self.getLevel())
			self.setDefensive(self.getDefensive())
			self.setFirstStrike(True)
		elif race == "Magier":
			newAtt = random.randint(3, 8)*self.getLevel()
			newDef = random.randint(3, 8)*self.getLevel()
			self.magierValue[0] = newAtt
			self.magierValue[1] = newDef
			self.setAttack(self.getAttack() + newAtt)
			self.setDefensive(self.getDefensive() + newDef)
			self.setFirstStrike(False)
		elif race == "Bogenschütze":
			self.setAttack(self.getAttack())
			self.setDefensive(self.getDefensive() + 5*self.getLevel())
			self.setFirstStrike(False)
		self.special = False

	def setSpecialAttackBack(self):
		race = self.getRace()
		if race == "Ork":
			self.setAttack(self.getAttack() - 5*self.getLevel())
			self.setDefensive(self.getDefensive())
			self.setFirstStrike(False)
		elif race == "Mensch":
			self.setAttack(self.getAttack() - 10*self.getLevel())
			self.setDefensive(self.getDefensive())
			self.setFirstStrike(False)
		elif race == "Magier":
			self.setAttack(self.getAttack() - self.magierValue[0])
			self.setDefensive(self.getDefensive() - self.magierValue[1])
			self.setFirstStrike(False)
		elif race == "Bogenschütze":
			self.setAttack(self.getAttack())
			self.setDefensive(self.getDefensive() - 5*self.getLevel())
			self.setFirstStrike(False)
		
	def getAttack(self):
		return self.att

	def setAttack(self, size):
		self.att = size

	def getDefensive(self):
		return self.defe

	def setDefensive(self, size):
		self.defe = size

	def getName(self):
		return self.name

	def setName(self, name):
		self.name = name

	def setName(self):
		self.name = raw_input("Gebe deinem Spieler einen Namen: ")

	def getLevel(self):
		return self.level

	def setLevel(self):
		self.level += 1

	def getRace(self):
		return self.race

	def setRace(self, race):
		if race == 0:
			self.race = "Ork"
		elif race == 1:
			self.race = "Mensch"
		elif race == 2:
			self.race = "Magier"
		elif race == 3:
			self.race = "Bogenschütze"
			
