#!/usr/bin/python
# -*- coding: utf-8 -*-

from Player import Player
from Color import Color
import random

class Game():
	def __init__(self):
		self.C = Color()
		self.player = []
		self.updateAttackUnit = {}
		self.updateAttackValue = {}
		self.updateDefensiveUnit = {}
		self.updateDefensiveValue = {}
		self.insertAttackUnit()
		self.insertDefensiveUnit()

		# Example of the Color class
		print self.C.create("green", "red") + "Hallo :D" + self.C.END

	def main(self):
		players = input("Wieviele Spieler möchtest du erstellen: ")
		for i in range(0, players):
			p = Player()
			p.createPlayer()
			self.player.append(p)

		roundCounter = 1
		whileBreaker = False
		while True:
			print self.C.create("green", "red") + "---------- Runde " + str(roundCounter) + " ----------" + self.C.END
			for p in self.player:
				print self.C.create("green", "black") + "Spieler " + p.getName() + " ist dran" + self.C.END
				# Set specialBack
				self.printPlayerInfo(p)
				if not p.getSpecialAttack():	
					p.setSpecialAttackBack()
					self.printPlayerInfo(p)
				action = 0
				activatedSpecial = False
				while (action < 2 or action == 3 or action == 6) and not activatedSpecial:
					action = input("Was möchtest du tun?\n[0]Anleitung lesen\n[1]Spieler Infos lesen\n[2]Upgrade\n[3]Spezial-Attacke\n[4]Kampf\n[5]Runde überspringen\n[6]Spiel Online speichern\n[7]Aufgeben (Spiel wird beendet!)\n")
					if action == 0:
						self.instructions()
					elif action == 1:
						self.printPlayerInfo(p)
					elif action == 2:
						print "Deine aktuellen Werte (Angriff/Verteidigung): " + str(p.getAttack()) + "/" + str(p.getDefensive())
						p.upgradePlayer()
						print "Neue Werte sind " + str(p.getAttack()) + "/" + str(p.getDefensive())
					elif action == 3:
						activatedSpecial = p.specialAttack()
						if activatedSpecial:
							p.setSpecialAttack()
							self.printPlayerInfo(p)
					elif action == 4:
						self.fight(p)
					elif action == 5:
						print "Runde wird übersprungen"
					elif action == 6:
						print "Spiel wird hochgeladen... to App Engine"
					elif action == 7:
						whileBreaker = True
						break
					else:
						print "Falsche auswahl... Abbruch"
				if whileBreaker:
					break				
			if whileBreaker:
				break
			roundCounter += 1
					
		else:
			print "---------- Spiel ende ----------"

	def fight(self, p):
		print "Suche dir einen Gegner aus:"
		ii = 0
		enemyList = {}
		for pl in self.player:
			print "[" + str(ii) + "]" + pl.getName()
			enemyList[ii] = pl
			ii += 1

		enemyValue = input()
		enemy = enemyList[enemyValue]
		print "Dein Genger ist " + enemy.getName()
		
		updateAttack = random.randint(0, len(self.updateAttackUnit)*3)
		updateDef = random.randint(0, len(self.updateDefensiveUnit)*3)
		
		updateA = False
		updateD = False
		if updateAttack < len(self.updateAttackUnit):
			print "Du hast Glück und findest einmalig für den Kampf und für den Angriff ein/eine: " + self.updateAttackUnit[updateAttack]
			updateAttack = self.updateAttackValue[updateAttack]
			updateA = True
		else:
			updateAttack = 0
		if updateDef < len(self.updateDefensiveUnit):
			print "Du hast Glück und findest einmalig für den Kampf und zur die Verteidigung ein/eine: " + self.updateDefensiveUnit[updateDef]
			updateDef = self.updateDefensiveValue[updateDef]
			updateD = True
		else:
			updateDef = 0

		if not updateA and not updateD:
			print "Deine Werte sind (Angriff/Verteidigung): " + str(p.getAttack()) + "/" + str(p.getDefensive())
		elif updateA and not updateD:
			print "Deine Werte sind (Angriff/Verteidigung): " + str(p.getAttack()) + "(+" + updateAttack + ")/" + str(p.getDefensive())
		elif not updateA and updateD:
			print "Deine Werte sind (Angriff/Verteidigung): " + str(p.getAttack()) + "/" + str(p.getDefensive()) + "(+" + updateDef + ")"
		elif updateA and updateD:
			print p.getName() + " Werte sind (Angriff/Verteidigung): " + str(p.getAttack()) + "(+" + updateAttack + ")/" + str(p.getDefensive()) + "(+" + updateDef + ")"

		print "Die Werte von " + enemy.getName() + " sind (Angriff/Verteidigung): " + str(enemy.getAttack()) + "/" + str(enemy.getDefensive())

		self.showdown(p, updateAttack, updateDef, enemy)
			
	def showdown(self, p, updateAttack, updateDef, enemy):
		firstStrike = random.randint(0, 1)
		playerAttack = p.getAttack() + int(updateAttack)
		playerDef = p.getDefensive() + int(updateDef)
		enemyDef = enemy.getDefensive()	

		while playerDef >= 0 and enemyDef >= 0:
			if firstStrike == 0:
				print self.C.create("green", "black") + p.getName() + " attackiert " + enemy.getName() + self.C.END
				enemyDef -= playerAttack
			else:
				print self.C.create("green", "black") + enemy.getName() + " attackiert " + p.getName() + self.C.END
				playerDef -= enemy.getAttack()

			print "Neue Wert von " + p.getName() + ": (" + str(p.getAttack()) + "/" + str(playerDef) + ")"
			print "Neue Wert von " + enemy.getName() + ": (" + str(enemy.getAttack()) + "/" + str(enemyDef) + ")"
				
			if firstStrike == 0:
				firstStrike = 1
			else:
				firstStrike = 0
	
		if playerDef <= 0:
			print self.C.create("green", "black") + p.getName() + " hat den Kampf verloren" + self.C.END
		else:
			print self.C.create("green", "black") + enemy.getName() + " hat den Kampf verloren" + self.C.END

	def instructions(self):
		print "Hier entsteht eine Anleitung ;)"

	def printPlayerInfo(self, p):
		print self.C.create("green", "red") + "Deine Name ist " + str(p.getName()) + ","
		print "Du spielst einen " + str(p.getRace()) + " auf Level " + str(p.getLevel())
		print "Dein Angriff ist " + str(p.getAttack()) + " und deine Verteidigung " + str(p.getDefensive()) + self.C.END

	def insertAttackUnit(self):
		units = ["Blatt-Papier", "Hamburger", "Schere", "Stein", "Steinschleuder", "Pistole", "Handgranate"]
		value = ["2", "3", "5", "8", "8", "9", "12"]

		i = 0
		for x in units:
			self.updateAttackUnit[i] = x
			self.updateAttackValue[i] = value[i]
			i += 1

	def insertDefensiveUnit(self):
		units = ["Blatt-Papier", "Takko", "Glas", "Holzplatte", "Tablet-PC", "Metall-Platte", "Adamantium-Schild"]
		value = ["2", "3", "5", "8", "8", "9", "12"]

		i = 0
		for x in units:
			self.updateDefensiveUnit[i] = x
			self.updateDefensiveValue[i] = value[i]
			i += 1

# Call Game-Class
G = Game()
G.main()
