#!/usr/bin/python
# -*- coding: utf-8 -*-

# Dies ist ein äöü test :)
print("testen wir auch hier äöü :)")

fObj = open("dictonary.txt", "r")

words = {}
for inhalt in fObj:
	inhalt = inhalt.strip()
	laender = inhalt.split(" ")
	words[laender[0]] = laender[1]

print("Vorhandene Länder: ")
for i in words:
	print("{}").format(i)
print("\n")

word = "Italy"
print("Beende mit 0 (null) eingabe!")
while word != "0":
	word = raw_input("Bitte Land wählen: ")
	if word in words:
		print("Richtig, das Lösungwort für {} ist: {}\n").format(word, words[word])
	elif word != "0":
		print("Leider nicht vorhanden")
		add = raw_input("Möchten Sie \"" + word + "\" hinzufügen? [J]a [N]ein: ")
		if add == "J":
			newKey = raw_input("Lösungswort zum Schlüssel: ")
			words[word] = newKey
			print("Hinzugefügt")
else:
	print("Beendet!")

fObj.close()				

fObj = open("dictonary.txt", "w")

for w in words:
	fObj.write("{} {}\n".format(w, words[w]))

fObj.close()
